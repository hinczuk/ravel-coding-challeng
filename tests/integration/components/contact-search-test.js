import Ember from 'ember';
import { moduleForComponent, test } from 'ember-qunit';
import wait from 'ember-test-helpers/wait';
import hbs from 'htmlbars-inline-precompile';

const contactServiceStub = Ember.Service.extend({
  currentContact: undefined,
    
  setupCurrentContact: function(contact) {
    return this.set('currentContact', contact); 
  }
});

moduleForComponent('contact-search', 'Integration | Component | contact search', {
  integration: true,
  beforeEach: function () {
    this.register('service:contacts-service', contactServiceStub);
    this.inject.service('contacts-service', { as: 'contactsService' });
  }
});

test('it renders', function(assert) {
  let stubContacts = [Ember.Object.create({
    firstName: 'Hilma',
    lastName: 'Mosciski',
    jobTitle: 'Investor',
    company: 'Bogan Inc',
    birthday: '2016-07-04T04:58:04.957Z',
    picture: 'https://s3.amazonaws.com/uifaces/faces/twitter/panghal0/128.jpg',
    email: 'Hilma.Mosciski9@gmail.com',
    phone: '11-888-4242-665'
  }), Ember.Object.create({
    firstName: 'Erik',
    lastName: 'Stur',
    jobTitle: 'Investor',
    company: 'Bogan Inc',
    birthday: '2016-07-04T04:58:04.957Z',
    picture: 'https://s3.amazonaws.com/uifaces/faces/twitter/panghal0/128.jpg',
    email: 'Hilma.Mosciski9@gmail.com',
    phone: '11-888-4242-665'
  })];

  this.set('contacts', stubContacts);
  this.set('currentContact', stubContacts[0]);

  this.set('externalSelectContact', (contact) => {
    this.get('contactsService').setupCurrentContact(contact);
  });

  this.render(hbs`{{contact-search 
    contacts=contacts
    currentContact=currentContact
    selectContact=(action externalSelectContact)
  }}`);

  assert.equal(this.$('li.contact.contact-item').length, 2, 'Displays 2 contacts');
  assert.equal(this.$('li.contact.active .information .name').text().trim(), 'Hilma Mosciski', 'Displays current contact name');

  this.$('li.contact.contact-item:last-child').click();

  return wait().then(() => {
    assert.equal(this.$('li.contact.contact-item.active .information .name').text().trim(), 'Erik Stur', 'Displays new current contact name');
  });
});
