import Ember from 'ember';
import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('contact-detail', 'Integration | Component | contact detail', {
  integration: true
});

test('it renders', function(assert) {
  let stubContact = Ember.Object.create({
    firstName: 'Hilma',
    lastName: 'Mosciski',
    jobTitle: 'Investor',
    company: 'Bogan Inc',
    birthday: '2016-07-04T04:58:04.957Z',
    picture: 'https://s3.amazonaws.com/uifaces/faces/twitter/panghal0/128.jpg',
    email: 'Hilma.Mosciski9@gmail.com',
    phone: '11-888-4242-665'
  });

  this.set('contact', stubContact);
  this.render(hbs`{{contact-detail contact=contact}}`);

  assert.equal(this.$('.name').text().trim(), `${stubContact.firstName} ${stubContact.lastName}`, 'Displays current contact name');
  assert.equal(this.$('.title-company').text().trim(), `${stubContact.jobTitle} at ${stubContact.company}`, 'Displays title company');
  assert.equal(this.$('.phone').text().trim(), stubContact.phone, 'Displays phone');
  assert.equal(this.$('.email').text().trim(), stubContact.email, 'Displays email');
  assert.equal(this.$('.birthday').text().trim(), stubContact.birthday, 'Displays birthday');
});

test('it switchs to editable', function(assert) {
  let contact = Ember.Object.extend({
    save: function() {
      this.set('email', 'newemail@asd.com');
    }
  });

  let stubContact = contact.create({
    firstName: 'Hilma',
    lastName: 'Mosciski',
    jobTitle: 'Investor',
    company: 'Bogan Inc',
    birthday: '2016-07-04T04:58:04.957Z',
    picture: 'https://s3.amazonaws.com/uifaces/faces/twitter/panghal0/128.jpg',
    email: 'Hilma.Mosciski9@gmail.com',
    phone: '11-888-4242-665'
  });

  this.set('contact', stubContact);
  this.render(hbs`{{contact-detail contact=contact}}`);

  assert.equal(this.$('.email').text().trim(), stubContact.email, 'Displays former email');

  this.$('.actions span.icon').click();

  this.$('.email input').val('newemail@asd.com');
  this.$('.email input').change();

  this.$('input.submit-button').click();

  assert.equal(this.$('.email').text().trim(), 'newemail@asd.com', 'Displays new email');
});
