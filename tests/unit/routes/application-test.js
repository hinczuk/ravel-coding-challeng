import { moduleFor, test } from 'ember-qunit';

moduleFor('route:application', 'Unit | Route | application', {});

test('should transition to contacts route', function(assert) {
  let route = this.subject({
    replaceWith(routeName) {
      assert.equal(routeName, 'contacts', 'replace with route name contacts');
    }
  });
  route.beforeModel();
});
