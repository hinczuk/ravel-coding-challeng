import Ember from 'ember';

const { Controller, inject: { service } } = Ember;

export default Controller.extend({
  contactsService: service(),

  isAddingContact: false,

  actions: {
    selectContact: function(contact) {
      this.set('isAddingContact', false);
      this.get('contactsService').setupCurrentContact(contact);
    },

    filterContacts(val) {
      let filtered = [];
      let contacts = this.model.contacts;

      if (val && val.trim() !== '') {
        filtered = contacts.filter((contact) => {
          return (contact.get('fullName').toLowerCase().indexOf(val.toLowerCase()) > -1);
        });
      } else {
        filtered = contacts;
      }

      return filtered;
    },

    toggleAddingContact() {
      this.set('isAddingContact', true);
    }
  }
});
