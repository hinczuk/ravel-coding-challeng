import Ember from 'ember';

const { Route, inject: { service }, RSVP: { hash } } = Ember;

export default Route.extend({
  contactsService: service(),

  model() {
    return hash({
      currentContact : this.get('contactsService').currentContact,
      contacts       : this.store.findAll('contact')
    });
  },

  afterModel: function(model) {
    if (!model.currentContact) {
      let firstContact = model.contacts.get('firstObject');
      this.get('contactsService').setupCurrentContact(firstContact);
      model.currentContact = firstContact;
    }
  }
});
