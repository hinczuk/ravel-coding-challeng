export default function() {
  // this.namespace = '';
  this.timing = 400;

  /*
    Route shorthand cheatsheet
  */
  this.get('/contacts');
  this.post('/contacts');
  this.get('/contacts/:id');
  this.patch('/contacts/:id');
}
