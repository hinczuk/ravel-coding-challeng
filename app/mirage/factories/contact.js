import Mirage, {faker}  from 'ember-cli-mirage';

export default Mirage.Factory.extend({
  firstName: faker.name.firstName,
  lastName: faker.name.firstName,
  jobTitle: faker.name.jobTitle,
  company(i) {
    return faker.company.companyName(`${i}`);
  },
  birthday: faker.date.past,
  picture: faker.image.avatar,
  email: faker.internet.email,
  phone(n) {
    return faker.phone.phoneNumber(`0800-111-213${n}`);
  }
});
