export default function(){
  this.transition(
    this.hasClass('toLeft-demo'),
    this.use('toLeft')
  );
}
