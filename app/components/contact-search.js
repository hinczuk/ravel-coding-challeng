import Ember from 'ember';

const { Component, inject: { service } } = Ember;

export default Component.extend({
  classNames: ['contact-search'],

  contactsService: service(),

  value: '',

  init() {
    this._super(...arguments);
    this.set('filteredContacts', this.get('contacts'));
  },

  actions: {
    handleSelectContact: function(contact) {
      let selectAction = this.get('selectContact');
      selectAction(contact);
      this.set('currentContact', contact);
    },

    handleFilterEntry: function() {
      let filterInputValue = this.get('value');
      let filterAction = this.get('filter');
      this.set('filteredContacts', filterAction(filterInputValue));
    },

    handleAddNewContactToggle: function() {
      this.get('toggleAddingContact')();
    }
  }
});
