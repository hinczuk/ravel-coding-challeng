import Ember from 'ember';

const { Component, inject: { service } } = Ember;

export default Component.extend({
  store: service(),

  classNames: ['contact-detail'],

  isEditing: false,

  actions: {
    toggleEdit() {
      this.toggleProperty('isEditing');
    },

    updateContact: function() {
      let updatedContact = this.getProperties('contact').contact;

      updatedContact.save();

      this.toggleProperty('isEditing');
    }
  }
});
