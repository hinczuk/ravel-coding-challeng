import Ember from 'ember';

const { Component, inject: { service } } = Ember;

export default Component.extend({
  store: service(),

  classNames: ['contact-detail'],

  isSaved: false,

  actions: {
    saveContact: function() {
      let contact = this.get('store').createRecord('contact', {
        picture: this.get('picture') || 'http://akashahamin.com/eduscience/resources/images/avatar/a0000.jpg',
        jobTitle: this.get('jobTitle'),
        company: this.get('company'),
        phone: this.get('phone'),
        email: this.get('email'),
        firstName: this.get('firstName'),
        lastName: this.get('lastName'),
        birthday: this.get('birthday')
      });
      contact.save();

      this.toggleProperty('isSaved');
    }
  }
});
