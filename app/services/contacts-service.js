import Ember from 'ember';

export default Ember.Service.extend({
  currentContact: undefined,
    
  setupCurrentContact: function(contact) {
    this.set('currentContact', contact); 
  }
});
